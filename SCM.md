## Actualización de software para nodos

#### Se necesita:
| SOFTWARE | IGUAL O SUPERIOR A |
| -------- | -------- |
| Mercurial   | 3.73   |
| Git   | 2.7.4   |
| Java  | Último    |
| Glibc o lib6  | 2.23 |

-------

#### En el Master (Nodo 1), se crea script en bash llamado *install_node_essential.sh* el cual contiene el siguiente codigo.
```
#!/bin/bash

clear
echo -e "\e[1;36m INFO:\e[1;32m Adding repo for scm packages(GIT & MERCURIAL) "
zypper addrepo http://download.opensuse.org/repositories/devel:/tools:/scm/SLE_$
zypper refresh
echo -e "\e[1;36m INFO:\e[1;32m Installing..."
echo -e "\e[1;36m INFO:\e[1;32m Installing GIT 2.9"
zypper install git -y
echo -e "\e[1;36m INFO:\e[1;32m Installing MERCURIAL"
zypper install mercurial -y
echo -e "\e[1;36m INFO:\e[1;32m Ending..."
```
#### Se le dan permisos, con el siguiente comando `chmod u+x install_node_essential.sh`
#### Luego se usa `scp` para copiarlo a los nodos, como se muestra a continuacion
#### `scp intall_node_essential.sh root@172.19.0.19X:/root/` Para los nodos a configurar.
#### Luego se corre en cada uno, ejecutando desde el home(`cd ~`) de cada nodo `./install_node_essential.sh`
#### Este codigo permite, añadir el repositorio que contiene Git y Mercurial a la maquina, y luego los instala.