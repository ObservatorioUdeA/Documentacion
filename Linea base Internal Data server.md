

Linea base Internal Data Server
==========================

Introducción
-------
Este documento contiene los pasos seguidos en la instalación de los diferentes componentes del servidor interno de datos del Observatorio de la Universidad de Antioquia. El servidor posee el sistema operativo Suse Linux Enterprice 12 service pack 1.


Mongo 3.2
------------
**Instalación** 
Con motivo de poseer un motor de base de datos no relacional en donde se pueda almacenar la información no estructurada del observatorio, se instala Mongo DB Community Edition. Para ello se siguieron los siguientes pasos:

Añadir el repositorio de Mongo
```bash
$ sudo zypper addrepo --no-gpgcheck https://repo.mongodb.org/zypper/suse/$(sed -rn 's/VERSION=.*([0-9]{2}).*/\1/p' /etc/os-release)/mongodb-org/3.2/x86_64/ mongodb
```

Actualizar el reppositorio
```bash
    $ sudo zypper refresh
``` 

Posteriormente se instalan los binarios de mongoDB junto con todas sus dependencias
```bash
    $ sudo zypper -n install mongodb-org
``` 
**Puesta en marcha del servicio**
Iniciando el servicio
```bash
    $ sudo service mongod start
``` 
El servicio debe iniciarse y esperar por conecciones a la base de datos.
La base de datos es un servicio que se espera siempre este funcionando, por lo que se realiza una  configuración adicional para que el servicio  inicie con el sistema, para ello se ejecuta el siguiente comando:
```bash
    $ sudo chkconfig mongod on
``` 
**Administrador de la BD**
Es necesario crear un administrador de todo el motor de bases de datos y un administrador por cada base de datos.
Al principio la base de datos no cuenta con ningún usuario por lo que hay que decirle al servicio que permita conexiones sin autenticación
```bash
    $ mongod --noauth
``` 
El administrador del motor de base de datos se crea ingresando al **localhost** por el puerto de conexiones utilizando el siguiente comando
```bash
    $ mongo --port 27017 --authenticationDatabase admin
``` 
Posteriormente se crea una base de datos para almacenar los administradores del motor, en este caso llamaremos a esa base de datos **admin**
```mongo
    use admin
```
Luego se crea el administrador del motor de base de datos
```mongo
    db.createUser(
	    {
		    user: "siteUserAdmin" ,
		    pwd: "password" ,
		    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
	    }
    )    
```
Para crear el administrador de una base de datos en específico se siguen los siguientes pasos.
Primero se crea/cambia a la base de datos, en este ejemplo la base de datos se llama **test**
```mongo
    use test
```
Luego se crea el usuario y se le dan permisos sobre esa base de datos
```mongo
    db.createUser(
	    {
		    user: "testAdmin",
		    pwd: "password",
		    roles: [ { role: "userAdmin", db: "test" } ]
	    }
    )
```
Por último se deshabilita la opción de iniciar sesión sin poseer una contraseña.
```bash
    $ mongod --auth
```

Postgres 9.5.4
--------------
**Instalación**
Se descarga el instalador de postgres en la página oficial para las distribuciones Linux_x86-64.
El paquete descargado posee como nombre **postgresql-9.5.4-1-linux-x64.run**, hay que darle permisos de ejecución al archivo
```bash
    $ chmod +755 postgresql-9.5.4-1-linux-x64.run
```
Posteriormente se ejecuta el instalador con el prefijo ./ y con permisos de superusuario.
```bash
    $ sudo ./postgresql-9.5.4-1-linux-x64.run
```
> **Nota:** En caso de que el paso anterior genere un error de ejecución al ejecutar **postgresql_installer_XXXXXXXXXX** se debe volver a montar la carpeta **/tmp** que es donde se ubica el instalador. La carpeta se monta con el siguiente comando: 
> 
> $ sudo mount -o remount --noexec /tmp
> 
> Dicho comando permitirá que los instaladores ubicados en la carpeta /tmp puedan ejecutarse

Posteriormente se solicita ingresar el directorio de instalación y el directorio de datos, se dejaron los valores en blanco para las opciones por defecto (/opt/Postgres/9.5/  , /opt/Postgres/9.5/data  respectivamente).

Al ingresar los directorios se solicita un usuario y contraseña para el superusuario de la base de datos (postgres). ES sumamente importante que no se olvide la contraseña y que se almacene en un lugar seguro, puesto que este usuario posee todos los permisos sobre la base de datos.

El puerto para conexiones se dejó en el puerto por defecto 5432, posteriormente será cambiado.

Luego se solicita ingresar el idioma local para la base de datos. En este caso se ingresa la opción **160** correspondiente al lenguaje del sistema operativo **es_CO.utf8**

Al finalizar las configuraciones anteriores el instalador procede a copiar los binarios de postgreSQL a las carpetas e iniciar el demonio. No obstante es posible que debido a un error de permisos  salga el error de la *Figura 1*

![image](assets/lineabaseInternalDataServer/errorPostgresInstall.jpg)
*Figura 1 Error en el inicio del servicio de la base de datos*

Es probable que el anterior error ocurriera debido a un problema con los permisos de usuario, para eso se debe cambiar el propietario de los binarios de PostgreSQL e iniciar el servicio de base de datos manualmente.
```bash
    $ sudo sudo chown -R postgres /opt/PostgreSQL
```
Adicionalmente es útil añadir el directorio de datos a las variables de entorno del sistema. En nuestro caso esto se realiza editando el archivo **/etc/environment** para que los cambios afecten a todos los usuarios, en el archivo se debe añadir la siguiente línea:

	PGDATA=/opt/PostgreSQL/9.5/data

**Iniciar el motor de base de datos**
Una buena práctica es iniciar el motor de base de datos desde la cuenta postgres, debido a que no es posible loguearse con un par usuario contraseña en esta cuenta, se debe ingresar logueandose primero en la cuenta root mediante el siguiente comando:
```bash
    $ su postgres
```
Luego se debe ejecutar el servidor mediante el comando:
```bash
    $ bash /opt/PostgreSQL/9.5/bin/initdb
```
O en caso de que no se desee loguear como el usuario postgres, también se puede emplear el siguiente comando siendo root:
```bash
    $ runuser -l postgres -c /opt/PostgreSQL/9.5/bin/initdb
```

> Written with [StackEdit](https://stackedit.io/).






