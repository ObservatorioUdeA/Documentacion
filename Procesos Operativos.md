

Procesos Operativos
===================

Introducción
------------

En este documento es una guía para aquellos desarrolladores, investigadores, auditores y demás personas o entidades académicas que deseen hacer uso de la información almacenada en el Observatorio de la Universidad de Antioquia o deseen subir información para ser procesada por el observatorio.

Objetivo
--------

Este documento pretende servir como guía a quienes deseen interactuar con la información disponible en el Observatorio de la Universidad de Antioquia, ya sea en el ingreso de información, en su consulta, o en su edición.

Consulta de instructivos y código
---------------------------------
El Observatorio de la Universidad de Antioquia cuenta con diferentes plataformas para almacenar información, en lo que respecta a documentos, instructivos, publicaciones, análisis, guías y códigos el Observatorio los almacena en la plataforma GitLab. 
Para acceder a la página principal del Observatorio se debe ingresar a la siguiente URL:
https://gitlab.com/groups/ObservatorioUdeA

Los diferentes proyectos poseen espacios separados dentro de la plataforma, dichos espacios son llamados Repositorios algunos son públicos y otros son de uso interno del observatorio Figura 1.

![image](assets/ProcesosOperativos/Gitlab.jpg)
*Figura 1 Página principal de repositorios/proyectos públicos del Observatorio de software.*

> **Nota**: Si usted necesita acceso a un repositorio privado, favor ponerse en contacto con el personal del Observatorio, sus datos de contacto se  pueden consultar en la pestaña **Members** de la página principal de los repositorios del proyecto así como lo muestra la *Figura 2*, para acceder a los datos de contacto de los usuarios, basta con hacer click sobre sus perfiles.

![image](assets/ProcesosOperativos/gitlab-members.jpg)
*Figura 2 Personal de contacto Observatorio de Software*

Este documento hace parte del repositorio nombrado **Documentación** y se accede mediante la página principal de repositorios (*Figura 1*). Al acceder a un proyecto aparece información básica del mismo y en la pestaña **Repository** aparecen los documentos como tal (*Figura 3*).
![image](assets/ProcesosOperativos/gitlab-documentacion.jpg)
*Figura 3 Documentos del proyecto Documentación*
 
Cada archivo sea un documento, código o análisis se encuentra en su última versión, no obstante es posible mirar versiones anteriores del mismo. Para descargar un archivo basta con hacer click sobre el proyecto y luego presionar el botón **Download zip**, inmediatamente todos los documentos del repositorio serán descargados.
Los documentos normalmente se almacenan en formato **Markdown** ( .md)  por lo que pueden ser visualizados directamente sin necesidad de  descargarlos, para visualizar un documento basta con hacer click sobre el *Figura 4.*
![image](assets/ProcesosOperativos/gitlab-preview.jpg)
*Figura 4 Descarga de un archivo del Proyecto Documentación.*

**Ingreso de nueva información**
----------------------------
Uno de los procesos mas importantes en el observatorios es el del ingreso de nueva información a este. El proceso posee los siguientes pasos:


Envio de informacion
--------------------

Las diferentes facultades pueden enviar información [estructurada][SQL]  o [no estructurada][NoSQL] al observatorio. En caso de que la información provenga de algún motor de base de datos, se puede crear un servicio web que exponga dicha información en formato JSON (*Ejemplo 1*).
```json
{
	"id": 1,
	"first_name": "Michelle",
	"last_name": "Stanley",
	"email": "mstanley0@slideshare.net",
	"gender": "Female",
	"ip_address": "139.150.113.202"
}
```
*Ejemplo 1 Información en formato JSON*

> **Nota:** Es muy importante que la información que se expone a travez los servicios web este cifrada para asegurar la confidencialidad de la información.

En caso de que la información que deseen enviar sean archivos, (encuestas, hojas de calculo, datos en formato CVS, JSON), se deben enviar al personal del observatorio por correo junto con las indicaciones para el trato de dicha información, como si es información para almacenar temporal o permanentemente, si es de dominio privado, entre otras características.

Recepcion de la informacion
---------------------------

Ya sea mediante un servicio web o mediante un correo el personal del Observatorio recibe la información que envió un área académica y procede a almacenarla.

Almacenamiento de la informacion
--------------------------------

En el caso de la [información no estructurada](#almacenar-informacion-no-estructurada) esta se lleva a una base de datos no relacional con diferentes colecciones de documentos, dicha base funciona en Mongo. Para la [información estructurada](#almacenar-informacion-estructurada) se  debe almacenar en una base de datos relacional, para esto se poseen diferentes bases de datos relacionales que funcionan en el motor PostgreSQL.
>**Nota:** Existe información de carácter temporal, dicha información no requiere persistencia en el observatorio y puede pasar directamente a Hadoop.

La información que se encuentra como documentos sera almacenada en un sistema de archivos común. La estructura del sistema de archivos es similar a la de Hadoop, Cada Facultad posee usuarios y cada usuario posee una contraseña y una carpeta para almacenamiento de documentos.  


Subida de información a Hadoop
------------------------------

Los algoritmos y analisis que se ejecutan en el Observatorio poseen Hadoop como fuente de información, por lo que la información que se vaya a analizar debe estar  allí. Es el personal del Observatorio quien se encarga de la subida de información a Hadoop.
>**Nota:** Cada facultad puede tener uno o varios usuarios quienes envían archivos al observatorio estos se conocen como los propietarios de un archivo. Al ser subidos a hadoop los archivos quedan accesibles para todas las demás facultades, no obstante solo el usuario y/o la facultad propietarios de un archivo pueden modificarlos.

La información y/o archivos se pueden consultar a travez del [**HUE**][hue] que es el servicio que permite interactuar con Hadoop, para ello cada Facultad posee determinados usuarios, cada uno con un espacio de almacenamiento y una contraseña.
Para ingresar al HUE hay que ir a su pagina principal e ingresar el usuario y contraseña correspondiente *Figura 5*.

![image](assets/ProcesosOperativos/hadoop-hue.jpg) 
*Figura 5 HUE pagina principal*

Luego de iniciar sesión en el HUE se abre la pagina principal, donde se encuentran todos los proyectos en ejecución. Para navegar en el sistema de archivos es necesario ingresar a la opción **Gestionar HDFS** *Figura 6*, luego es posible cargar y/o eliminar archivos dentro de la carpeta de cada usuario, en la *Figura 7* se observan los archivos del usuario *userhdp*

![image](assets/ProcesosOperativos/hue-Acceshdfs.jpg) 
*Figura 6 HUE Gestionar HDFS*

![image](assets/ProcesosOperativos/hue-userhdpFS.jpg) 
*Figura 7 Carpeta de archivos del usuario userhdp*


Procesamiento de la información
-------------------------------

Luego de que la información este almacenada en Hadoop queda disponible para que su procesamiento, dicho procesamiento se hace principalmente mediante Spark, para esto el Observatorio cuenta con **Zeppelin** (*Figura 8*), el cual es un entorno web para ejecutar algoritmos de Spark.

![image](assets/ProcesosOperativos/zeppelin-home.jpg) 
*Figura 8 Zeppelin pagina principal*

En la sección de la derecha aparecen los "notebooks" o códigos almacenados en Zepellin, para editar o ejecutar un código basta con hacer click sobre este, en la *Figura 9* se muestra el codigo de ejemplo que trae Zeppelin ejecutando código JAVA y SQL.

![image](assets/ProcesosOperativos/zeppelin-tutorial.jpg) 
*Figura 9 Zeppelin codigo ejemplo en el editor*

En el siguiente [enlace][sparkSample] se pueden encontrar algunos ejemplos básicos de Spark.

Almacenamiento de los resultados
--------------------------------

Principalmente los resultados obtenidos se almacenan en Hadoop en la carpeta del usuario que ejecuto el código para el analisis, y pueden ser accedidos mediante el [HUE][hue] (*Figura 7*). 
Dado que Hadoop es un espacio de almacenamiento temporal los resultados deben ser extraídos, posiblemente también sean almacenados en las plataformas persistentes del observatorio (Mongo, PostgreSQL, sistema de archivos).

Publicacion de los resultados en el micrositio
----------------------------------------------

Los resultados obtenidos del procesamiento de datos en el Observatorio generalmente están dirigidos a su publicación, no obstante es posible que exista información privada que no deba publicarse, en estos casos los propietarios de los resultados deben especificar que se puede publicar y que no. En caso de que no aparezcan restricciones para la publicación de un conjunto de resultados, se considerara  información de dominio público.

**Almacenar informacion no estructurada**
-------------------------------------

Los datos no estructurados o débilmente estructurados como fotos, comentarios, o variables estadísticas de las que no se posee una correlación son el tipo de información que se almacena en las colecciones de la base de datos no estructurada de Mongo. Para este tipo de datos, el [Observatorio][gitlab] cuenta con algunas plantillas en formato JSON para la recepción de información no estructurada. A continuación se muestra una plantilla de recepción de información.

```json
{
	  "_id":"6AF345FAB1C2C3",
  "facultad":"Medicina",
  "nombre":"Encuesta para la permanencia 2012-2014",
  "fechaRecepcion":"22-11-2015",
  "variables":[
			  {nombre:"descripcion"},
			  {edad:"variable numerica entera que determina la edad del estudiante"},
  "datos":[1,"julian",";",2,"Rebecca","Taylor","rtaylor0@flavors.me","Female"]
}
```
*Ejemplo plantilla de recepcion de informacion no estructurada*
Almacenar informacion estructurada
-------------------------------------


   [NoSQL]: <http://www.lantares.com/blog/informacion-no-estructurada-lo-que-nos-ensenan-los-datos>
   [SQL]: <https://gestiondocumentalparagentenormal.com/2012/12/17/informacion-estructurada/>
   [hue]: <http://172.19.0.195:8888>
   [sparkSample]: <http://spark.apache.org/examples.html>
   [gitlab]: <https://gitlab.com/groups/ObservatorioUdeA>


> Written with [StackEdit](https://stackedit.io/).

