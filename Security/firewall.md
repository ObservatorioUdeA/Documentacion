
Firewall
===================

Salvar la configuración de Firewall por defecto
-------------
Primero, corremos el script **Export_Iptables.sh**, el cual debe tener permisos de ejecución. Se los asignamos de la siguiente manera.
```
sudo chmod +x Export_Iptables.sh
``` 
Y luego lo corremos:
```
sudo ./Export_Iptables.sh
```
Debería dar el siguiente resultado
```
./Export_Iptables.sh: línea 3: /etc/init.d/iptables: No existe el fichero o el directorio
./Export_Iptables.sh: línea 4: error sintáctico cerca del elemento inesperado `"IPtables saved at /tmp/ directory, the services was stopped"'
./Export_Iptables.sh: línea 4: `echo("IPtables saved at /tmp/ directory, the services was stopped")'
```
Guardando así un archivo llamado **iptables-export** en la carpeta **/tmp/**, el cual contiene todas nuestras políticas del Firewall.


----------


Detener el  Firewall 
-------------
Luego detenmos el servicio de Firewall con el comando ```/sbin/rcSuSEfirewall2 stop```, y cuando revisamos nuestras politicas actuales debemos obtener algo como esto: 
```
$ iptables -L
Chain INPUT (policy ACCEPT)
target     prot opt source               destination         

Chain FORWARD (policy ACCEPT)
target     prot opt source               destination         

Chain OUTPUT (policy ACCEPT)
target     prot opt source               destination 
```


> Written with [StackEdit](https://stackedit.io/).